import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '/src/assets/css/cssreset.css';
import Vant from 'vant';
import 'vant/lib/index.css';

import VueLuckyCanvas from '@lucky-canvas/vue'

createApp(App).use(Vant).use(VueLuckyCanvas).use(store).use(router).mount('#app')
