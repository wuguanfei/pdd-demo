import { createRouter, createWebHashHistory } from 'vue-router'
const home = () => import('@/views/home.vue');
const money = () => import('@/views/money.vue');

const routes = [
	{
		path: '/',
		name: 'home',
		component: home
	},
	{
		path: '/money',
		name: 'money',
		component: money
	},
]

const router = createRouter({
	history: createWebHashHistory(),
	routes
})

export default router
